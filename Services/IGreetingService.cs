﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAspNetCore.Services
{
    public interface IGreetingService
    {
        public string Greet(string name = null);
    }
}
