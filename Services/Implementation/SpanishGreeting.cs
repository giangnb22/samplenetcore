﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAspNetCore.Services.Implementation
{
    public class SpanishGreeting : IGreetingService
    {
        public string Greet(string name = null)
        {
            return $"Hola {name ?? "amigo"}! Como estas?";
        }
    }
}
