﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAspNetCore.Services.Implementation
{
    public class EnglishGreeting : IGreetingService
    {
        public string Greet(string name = null)
        {
            return $"Hello {name??"buddy"}! How are you?";
        }
    }
}
