﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SampleAspNetCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAspNetCore.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IGreetingService _greeting;

        public IndexModel(ILogger<IndexModel> logger, IGreetingService greeting)
        {
            _logger = logger;
            _greeting = greeting;
        }

        public string Greet() 
        {
            return _greeting.Greet();
        }

        public void OnGet()
        {

        }
    }
}
